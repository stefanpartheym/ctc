################################################################################
# ctc -- Copy to clipboard
################################################################################

TARGET                 := ctc.exe

SRC_DIR                := .
OBJ_DIR                := obj

MAIN                   := main.c
SOURCES                := 
OBJECTS                := $(SOURCES:%.c=%.o)
OBJ                    := $(MAIN:%.c=$(OBJ_DIR)/%.o) $(OBJECTS:%=$(OBJ_DIR)/%)

CFLAGS_COMMON          := -Wall -std=c99 -pedantic -pedantic-errors
CFLAGS                 := -g $(CFLAGS_COMMON) -D DEBUG
CFLAGS_RELEASE         := $(CFLAGS_COMMON)
LDFLAGS                := 

MKDIR                  := mkdir -p
GDB                    := gdb


# ------------------------------------------------------------------------------
# BUILD (main target)

# default target
default: debug

debug: build

release: CFLAGS := $(CFLAGS_RELEASE)
release: build

build: object_dir $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

# build object files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

# create object file directory
object_dir: $(OBJ_DIR)/
$(OBJ_DIR)/:
	$(MKDIR) $@

.PHONY: default debug release build object_dir


# ------------------------------------------------------------------------------
# EXECUTION

# execution of main target
run: build
	@./$(TARGET)

rund:     rundebug
rundebug: build
	$(GDB) ./$(TARGET)

.PHONY: run rund rundebug


# ------------------------------------------------------------------------------
# CLEANUP

# cleanup main target
clean:
	$(RM) $(TARGET) $(OBJ)

rebuild: clean default

.PHONY: clean rebuild
