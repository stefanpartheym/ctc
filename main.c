/*******************************************************************************
 * ctc -- Copy to clipboard
 * Simple clipboard tool for MinGW/MSYS.
 ******************************************************************************/

#include <string.h>
#include <wchar.h>
#include <windows.h>


/*
 * Copy a data string to the Windows clipboard as UTF-8 string.
 */
void copy_to_clipboard(const char* data)
{
    const size_t length = strlen(data) + 1;
    /* determine wide string buffer length */
    int buffer_length   = MultiByteToWideChar(CP_UTF8, 0, data, length, NULL, 0);
    /* allocate wide string buffer */
    wchar_t* buffer     = malloc(buffer_length + 1);
    memset(buffer, 0, buffer_length + 1);
    
    /*
     * Convert multi-byte string to utf-8 encoded wide string
     * 
     * TODO:
     * Check return value of function call MultiByteToWideChar() to chatch
     * possible conversion errors.
     */
    MultiByteToWideChar(CP_UTF8, 0, data, length, buffer, buffer_length);
    
    HGLOBAL h_mem = GlobalAlloc(GMEM_MOVEABLE, buffer_length * sizeof(wchar_t));
    memcpy(GlobalLock(h_mem), buffer, buffer_length * sizeof(wchar_t));
    GlobalUnlock(h_mem);
    OpenClipboard(0);
    EmptyClipboard();
    SetClipboardData(CF_UNICODETEXT, h_mem);
    CloseClipboard();
}


/* -------------------------------------------------------------------------- */

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
        char* data = argv[1];
        if (strlen(data) > 0)
            copy_to_clipboard(data);
    }
    
    return 0;
}
